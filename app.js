var express = require('express')
  , leilaoController = require('./controllers/Leilao')
  , orcamentoController = require('./controllers/Orcamento')
  , restLeilaoController = require('./controllers/RestLeilao')
  , restOrcamentoController = require('./controllers/RestOrcamento')
  , restItemOrcamentoController = require('./controllers/RestItemOrcamento')
  , http = require('http')
  , path = require('path');

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('_hostname', 'http://localhost:3000');
  app.set('views', __dirname + '/views');
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser());
  app.use(express.session({
    secret: "notagoodsecret",
    cookie: {httpOnly: true, secure: false},
  }));
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Rotas rest
app.post('/rest/leilao/criar', restLeilaoController.criar);
app.post('/rest/orcamento/criar', restOrcamentoController.criar);

app.get('/rest/leilao/:leilao_numero', restLeilaoController.obter);
app.get('/rest/leilao/:leilao_numero/item', restLeilaoController.obterComItens);

app.put('/rest/leilao/iniciar', restLeilaoController.iniciar);
app.put('/rest/leilao', restLeilaoController.alterar);

app.get('/rest/leilao/ranking/:leilao_numero', restLeilaoController.obterRanking);
app.get('/rest/itemorcamento/:orcamento_numero', restItemOrcamentoController.obterItensPorOrcamento);


// Rotas do site
app.get('/', leilaoController.criar);
app.get('/leilao/criar', leilaoController.criar);
app.get('/leilao/iniciar/:leilao_numero', leilaoController.iniciar);
app.get('/leilao/confirmacao/:leilao_numero', leilaoController.confirmacao);
app.get('/leilao/ranking/:leilao_numero', leilaoController.ranking);
app.get('/orcamento/criar/:leilao_numero', orcamentoController.criar);

global._app = app;

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
