var modelLeilao = require('../models/Leilao');


exports.criar = function(req, res) {
	var resultado = modelLeilao.criar(req.body.itens, function(resultado){
		res.send(resultado);
	});
};

exports.alterar = function(req, res) {
	modelLeilao.alterar(req.body, function(resultado){
		res.send(resultado);
	});
};

exports.obter = function(req, res) {
	var resultado = modelLeilao.obter(req.params.leilao_numero, function(resultado){
		res.send(resultado);
	});
};

exports.obterComItens = function(req, res) {
	var resultado = modelLeilao.obterComItens(req.params.leilao_numero, function(resultado){
		res.send(resultado);
	});
};

exports.iniciar = function(req, res) {
	modelLeilao.iniciar(req.body, function(resultado){
		res.send(resultado);
	});
};

exports.obterRanking = function(req, res) {
	modelLeilao.obterRanking(req.params.leilao_numero, function(resultado){
		res.send(resultado);
	});
};
