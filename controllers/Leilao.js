var modelLeilao = require('../models/Leilao');

exports.criar = function(req, res) {
	res.render('leilao/criar', {contexto: 'leilao_iniciar'});
};

exports.iniciar = function(req, res) {
	res.render('leilao/iniciar', {leilao_numero:req.params.leilao_numero, contexto: 'leilao_iniciar'});
};

exports.confirmacao = function(req, res) {
	res.render('leilao/confirmacao', {leilao_numero:req.params.leilao_numero, contexto: 'leilao_confirmacao'});
};

exports.ranking = function(req, res) {
	res.render('leilao/ranking', {leilao_numero:req.params.leilao_numero, contexto: 'leilao_ranking'});
};