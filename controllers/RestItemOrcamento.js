var modelItemOrcamento = require('../models/ItemOrcamento');


exports.obterItensPorOrcamento = function(req, res) {
	modelItemOrcamento.obterItensPorOrcamento(req.params.orcamento_numero, function(resultado) {
		res.send(resultado);
	});
};
