function CriarCtrl($scope, $http) {
	
	var s = $scope;

	 /**
	 * @property int orcamento_numero
	 * @property date orcamento_data
	 *
	 * @property int leilao_numero
	 * @property date leilao_inicio
	 * @property date leilao_fim
	 * @property date leilao_fimestimado
	 * @property date leilao_emails
	 * @property item_id 
	 * @property item_nome 
	 * @property item_quantidade 
	 * @property itemorcamento_valortotal
	 * @property itemorcamento_data 
	 */
	 s.orcamento = {itens: s.itens};
	 s.leilao = {};
	 s.valorTotal = 0;

	 jq = jQuery;

	 var Init = function(leilao) {
		s.obterLeilaoComItens(jq('#leilao_numero').val());
	};

	s.somar = function() {
		s.valorTotal = 0;
		for (var i = s.orcamento.itens.length - 1; i >= 0; i--) {
			if (jQuery.isNumeric(s.orcamento.itens[i].itemorcamento_valortotal)){
				s.valorTotal =  s.valorTotal + parseFloat(s.orcamento.itens[i].itemorcamento_valortotal);
			}
		}
	};

	s.obterLeilaoComItens = function(leilao_numero){
		$http.get('/rest/leilao/'+leilao_numero+'/item').success(function(res){
			if (res.s) {
				s.orcamento.itens = res.r;
				if (res.r.length > 0) {
					s.leilao.leilao_localentrega = res.r[0].leilao_localentrega;
					s.leilao_fimestimado_format = moment(res.r[0].leilao_fimestimado).format('DD/MM/YYYY HH:mm');
					jQuery("#localEntrega").geocomplete({
					  map: ".mapa",
					  location: res.r[0].leilao_localentrega
					});
				}
			}else{
				window.alert(res.msg);
			}
		});
	};

	s.enviar = function(){
		var orcamentoSemHashKey = {};
		angular.copy(s.orcamento,orcamentoSemHashKey);
		for (var i = orcamentoSemHashKey.itens.length - 1; i >= 0; i--) {
			// Somente os dados que serão salvos
			orcamentoSemHashKey.itens[i] = {
				itemorcamento_valortotal: orcamentoSemHashKey.itens[i].itemorcamento_valortotal,
				item_id: orcamentoSemHashKey.itens[i].item_id,
			}
		};
		$http.post('/rest/orcamento/criar', orcamentoSemHashKey)
		.success(function(res, status, headers, config){
			if (res.s) {
				window.location = '/leilao/ranking/'+jq('#leilao_numero').val();
			}else{
				window.alert(res.msg);
			}
		});
	};

	Init();
};