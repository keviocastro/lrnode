function IniciarCtrl($scope, $location, $http) {
	
	var s = $scope;
	s.itens = [];
	/**
	 * @property string item_nome
	 * @property int item_quantidade
	 * @property string item_descricao
	 */
	 s.item = {};

	/**
	 * @property int leilao_numero
	 * @property date leilao_inicio
	 * @property date leilao_fim
	 * @property date leilao_fimestimado
	 * @property date leilao_emails
	 * @property date pessoa_id
	 */
	 s.leilao = {};
	 s.estadoIniciar = 'enabled';
	 s.fimEstimadoFormat = null;

	 jq = jQuery;

	 var Init = function(leilao) {
		s.obterLeilao(jq('#leilao_numero').val());
	};

	s.iniciar = function(){
		if (s.estadoIniciar == 'enabled') {
			jq('#btnIniciar').text('Iniciando...');
			s.estadoIniciar = 'disabled';
			s.leilao.leilao_localentrega = jq('#inputLocalEntrega').val();

			$http.put('/rest/leilao/iniciar', s.leilao)
			.success(function(res) {
				if (res.s) {
					window.location = '/leilao/confirmacao/'+res.r.leilao_numero;
				}else{
					window.alert(res.msg);
				}
			});
		}
		
	};

	s.definirEncerramento = function(numHoras){
		var hora = moment(new Date());
		hora.add('hours', numHoras);
		s.fimEstimadoFormat = ' ~ '+hora.calendar();
		s.leilao.leilao_tempo = numHoras;
	};

	s.obterLeilao = function(leilao_numero){
		$http.get('/rest/leilao/'+leilao_numero)
		.success(function(res) {
			if (res.s) {
					s.leilao = res.r;
					jq('#btnTimeDefault').button('toggle');
					s.definirEncerramento(6); // 6 horas como padrão
				}else{
					window.alert(res.msg);
				}
		});
	};

	Init();
};