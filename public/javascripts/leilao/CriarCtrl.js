function CriarCtrl($scope, $http) {
	
	var s = $scope;
	s.itens = [];
	/**
	 * @property string item_nome
	 * @property int item_quantidade
	 * @property string item_descricao
	 */
	s.item = {};

	/**
	 * @property int leilao_numero
	 * @property date leilao_inicio
	 * @property date leilao_fim
	 * @property date leilao_fimestimado
	 * @property date leilao_emails
	 * @property date leilao_pagamento
	 */
	s.leilao = {itens: s.itens};
	s.stateSave = 'disabled';

	jq = jQuery;

	var Init = function() {

	};

	s.add = function() {
		if ( s.form.$valid ) {
			s.itens.push(angular.copy(s.item));
			s.item = {};
		}

		s.stateSave = ((s.itens.length > 0) ? 'enabled' : 'disabled');
	};

	s.del = function(item) {
		var index = s.itens.indexOf(item);
		s.itens.splice(index, 1);

		s.stateSave = ((s.itens.length > 0) ? 'enabled' : 'disabled');
	};

	s.salvar = function() {
		var leilaoSemHashKey = {};
		angular.copy(s.leilao,leilaoSemHashKey);

		if ( s.stateSave == 'enabled'){
			s.stateSave = 'disabled';
			jq('#btnCriar').text('Salvando leilão...');
			if ( s.itens.length > 0 ) {
				$http.post('/rest/leilao/criar', leilaoSemHashKey)
				.success(function(res) {
					if (res.s) {
						// console.log(res.r);
						window.location = '/leilao/iniciar/'+res.r.leilao_numero;
					}else{
						s.stateSave = 'enabled';
						window.alert(res.msg);
					}
				});
			};
		}
	};

	Init();
};