var db = require('./../config/Db');
var moment = require('moment');
var check = require('validator').check;
var emailServer   = require("../config/Email");
var ItemModel = require('./Item');
var util = require('util');
var _ = require('underscore')._;


var LeilaoDb = db.extend({
	tableName: "leilao"
});
var modelLeilao = new LeilaoDb();

var ItemDb = db.extend({
	tableName: "item"
});


exports.criar = function(itens, callback) {
	var resultado = {};

		var leilao = {
		'leilao_inicio': moment().format('YYYY-MM-DD HH:mm:ss'),
		'leilao_fim': null,
		'leilao_fimestimado': null
	};

	// Cria o leilão
	var novoLeilao = new LeilaoDb(leilao);
	novoLeilao.save(function(err, resultSaveLeilao){
		if (err) {
			resultado.s = false;
			resultado.msg = 'Erro ao criar o leilão';
			resultado.erro = err;

			callback(resultado);
		}else{
			//Cria os itens do leilão
			leilao.leilao_numero = resultSaveLeilao.insertId;
			for (var i = itens.length - 1; i >= 0; i--) {
				itens[i].leilao_numero = resultSaveLeilao.insertId;
				var novoItem = new ItemDb(itens[i]);

				novoItem.save(function(err, resultSaveLeilao){
					if (err) {
						resultado.s = false;
						resultado.msg = 'Erro ao criar os itens do leilão';
						resultado.erro = err;

						callback(resultado);
					}else{
						resultado.s = true;
						resultado.r = leilao;
						resultado.msg = 'O leilão foi criado';
						
						callback(resultado);
					}
				});

			};
			
		}
	});
	
};

exports.obter = function(leilao_numero, callback) {
	var leilao = new LeilaoDb();
	var resultado = {};

	leilao.find('first', {where: "leilao_numero = "+leilao_numero}, function(err, row) {
        if (err) {
        	resultado.s = false;
        	resultado.msg = "Erro ao obter o leilão";
        	resultado.erro = err;
        	callback(resultado);
        }else{
        	resultado.s = true;
        	resultado.r = row;
        	callback(resultado);
        }
	});
};

exports.obterComItens = function(leilao_numero, callback) {
	var select = "SELECT * ";
	select += " FROM `leilao` INNER JOIN `item` ON leilao.leilao_numero = item.leilao_numero ";
	select += " WHERE (leilao.leilao_numero = "+leilao_numero+")";

	var resultado = {};
	modelLeilao.query(select, function(err, rows, fields) {
		if (err) {
			resultado.s = false;
			resultado.msg = "Erro ao obter o leilao com itens";
			resultado.erro = err+'. Sql: '+select;
			callback(resultado);
		}else{
			resultado.s = true;
			resultado.r = rows;
			callback(resultado);
		}
	})
	
};

exports.iniciar = function(leilao, callback) {
	var resultado = {};
	leilao.leilao_fimestimado = moment().add('hours', leilao.leilao_tempo).format('YYYY-MM-DD HH:mm:ss');
	var leilaoModel = new LeilaoDb(leilao);
	
	leilaoModel.save('leilao_numero = '+ leilao.leilao_numero, function(err, resultUpdateLeilao) {
		if (err) {
			resultado.s = false;
			resultado.erro = err;
			resultado.msg = 'Erro ao iniciar o leilão';

			callback(resultado);
		}else{
			
			ItemModel.obter(leilao.leilao_numero, function(resultItens) {
				if (resultItens.s) {
					leilao.itens = resultItens.r;
					_app.render('orcamento/email/solicitacao',{'leilao':leilao, hostname: _app.get('_hostname')}, function(err, html) {
						if (!err) {
							if (_.isString(leilao.leilao_emails)) {
								var arrayEmail = leilao.leilao_emails.split(',');
								var email = '';
								for (var i = arrayEmail.length - 1; i >= 0; i--) {
									email = arrayEmail[i].trim();
									try{
										check(email).isEmail();
										emailServer.send({
											text: 'Solicitação de orçamento',
											from: 'Leilão <testenode@gmail.com>',
											to: email,
											subject: "Solicitação de orçamento",
											attachment: 
										   [
										      {data:html, alternative:true}
										   ]
										}, function(err, msg) {
											if (err) {
												util.debug('Erro ao enviar email de solicitação de orçamento: '+err);
											}else{
												util.debug('Email de solicitação enviada para. '+msg.header.to);
											}
										});
									}catch(e){
										util.debug('Email inálido. '+email);
									}
								}
							}
						}else{
							util.debug('Erro ao renderizar a view orcamento/email/solicitacao. Error: '+err);
						}
					});
				}else{
					return resultItens;
				}
			});

			resultado.s = true;
			resultado.msg = 'O leilão foi iniciado';
			resultado.r = leilao;
			callback(resultado);
		}
	});
};

exports.obterRanking = function(leilao_numero,  callback) {
	var select = "SELECT sum(itemorcamento.itemorcamento_valortotal) AS `totalorcamento`,\
			`itemorcamento`.`orcamento_numero`, `item`.`leilao_numero`, orcamento.*, leilao.* \
			FROM `item` INNER JOIN `itemorcamento` ON itemorcamento.item_id = item.item_id \
						INNER JOIN orcamento ON orcamento.orcamento_numero = itemorcamento.orcamento_numero \
						INNER JOIN leilao ON leilao.leilao_numero = item.leilao_numero \
			WHERE (item.leilao_numero = "+leilao_numero+") GROUP BY `itemorcamento`.`orcamento_numero` \
			ORDER BY `totalorcamento` ASC";
	
	var resultado = {};
	modelLeilao.query(select, function(err, rows, fields) {
		if (err) {
			resultado.s = false;
			resultado.msg = "Erro ao obter o ranking do leilão";
			resultado.erro = err;
			callback(resultado);
		}else{
			resultado.s = true;
			resultado.r = rows;
			callback(resultado);
		}
	})
};

exports.alterar = function(leilao, callback) {
	var resultado = {};
	var leilaoModel = new LeilaoDb(leilao);

	leilaoModel.save('leilao_numero = '+ leilao.leilao_numero, function(err, resultUpdateLeilao) {
		if (err) {
			resultado.s = false;
			resultado.erro = err;
			resultado.msg = 'Erro ao atualizar o leilão';
			callback(resultado);
		}else{
			resultado.s = true;
			resultado.msg = 'O leilão foi atualizado';
			resultado.r = leilao;
			callback(resultado);
		}
	});
};