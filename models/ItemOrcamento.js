var db = require('./../config/Db');
var moment = require('moment');

var orcamentoDb = db.extend({
	tableName: "itemorcamento"
});
var orcamentoModel = new orcamentoDb();

var ItemModel = require('./Item');

exports.obterItensPorOrcamento = function(orcamento_numero, callback) {
	var select = "SELECT itemorcamento.*, item.* FROM itemorcamento \
		INNER JOIN item ON item.item_id = itemorcamento.item_id \
		INNER JOIN orcamento ON orcamento.orcamento_numero = itemorcamento.orcamento_numero \
		WHERE (itemorcamento.orcamento_numero = "+orcamento_numero+")";
	var resultado = {};
	
	orcamentoModel.query(select, function(err, rows, fields) {
		if (err) {
			resultado.s = false;
			resultado.erro = err;
			resultado.msg = "Erro ao obter itens do ormcamento "+orcamento_numero;
			callback(resultado);
		}else{
			resultado.s = true;
			resultado.r = rows;
			callback(resultado);
		}
	});


};

