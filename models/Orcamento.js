var db = require('./../config/Db');
var moment = require('moment');
var email = require('emailjs');
var leilaoModel = require('./Leilao');

var OrcamentoDb = db.extend({
	tableName: "orcamento"
});

var ItemOrcamentoDB = db.extend({
	tableName: "itemorcamento"
});

exports.criar = function(orcamento, callback) {
	var resultado = {};
	var itens = orcamento.itens;
	delete orcamento.itens;

	var dataAtual =  moment().format('YYYY-MM-DD HH:mm:ss');

	orcamento.orcamento_data = dataAtual;
	orcamento.orcamento_valorentrega = orcamento.orcamento_valorentrega.replace(',', '.');
		
	if (itens.length < 1) {
		resultado.s = false;
		resultado.msg = 'Erro ao criar o orçamento';
		resultado.erro = 'Não foi definido o valor dos itens';

		callback(resultado);
	};

	// Cria o leilão
	var novoOrcamento = new OrcamentoDb(orcamento);
	novoOrcamento.save(function(err, resultSaveOrcamento){
		if (err) {
			resultado.s = false;
			resultado.msg = 'Erro ao criar o orçamento';
			resultado.erro = err;

			callback(resultado);
		}else{
			//Cria os itens do leilão
			orcamento.orcamento_numero = resultSaveOrcamento.insertId;
			for (var i = itens.length - 1; i >= 0; i--) {
				itens[i].orcamento_numero = resultSaveOrcamento.insertId;
				itens[i].itemorcamento_data = dataAtual;
				itens[i].itemorcamento_valortotal = itens[i].itemorcamento_valortotal.replace(',', '.');
				var novoItem = new ItemOrcamentoDB(itens[i]);

				novoItem.save(function(err, resultSaveOrcamento){
					if (err) {
						resultado.s = false;
						resultado.msg = 'Erro ao criar os itens do orçamento';
						resultado.erro = err;

						callback(resultado);
					}
				});
			};
			resultado.s = true;
			resultado.r = orcamento;
			resultado.msg = 'O orçamento foi criado';
			
			callback(resultado);
		}
	});
	
};