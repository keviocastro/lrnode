var db = require('./../config/Db');
var ItemDb = db.extend({
	tableName: "item"
});

var ItemModel = new ItemDb();

exports.obter = function(leilao_numero, callback) {
	var resultado = {};

	ItemModel.find('all', {where:"leilao_numero = "+leilao_numero}, function(err, rows) {
		if (err) {
			resultado.s = false;
			resultado.erro = err;
			resultado.msg = "Erro ao obter itens do leilão";
			callback(resultado);
		}else{
			resultado.s = true;
			resultado.r = rows;
			callback(resultado);
		}
	});
};